package com.example.learningforfun.common.view

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.learningforfun.adapter.LabelAdapter
import com.example.learningforfun.model.Label

class MyRecyclerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = androidx.recyclerview.R.attr.recyclerViewStyle
) : RecyclerView(context, attrs, defStyleAttr) {
    //List data for recyclerView
    //This is an list of Label object (just for demo)
    private var list: List<Label>? = null

    /**
     * This function input a list of Label object
     * @param: inputList - List of Label (It's can be ArrayList, MutableList, etc)
     * @return void
     */
    fun inputList(inputList: List<Label>) {
        this.list = inputList
    }

    /**
     * Override function setAdapter
     * @param adapter : Adapter for RecyclerView
     * @return void
     */
    override fun setAdapter(adapter: Adapter<*>?) {
        super.setAdapter(adapter)
        //After set adapter for RecyclerView, adapter will submit the inputList
        //So can the layout calculate how many item are visible on the recyclerView
        (adapter as LabelAdapter).submitList(list)

        //After the recyclerView finish initialize
        post {
            //This is a current list data of Adapter
            var currentList = list
            if (currentList!!.size - 1 <= (layoutManager as LinearLayoutManager).findLastVisibleItemPosition()) {
                //If the last visible position smaller or equals size of currentList
                //do nothing
            } else {
                /* Else: x3 the currentList
                 * Pros:
                 *  Make the calculation when scroll infinite much more smooth (reduce lag when scroll)
                 * Cons:
                 *  May affect to perform if there to much item
                 */
                currentList = list!! + list!! + list!!
                adapter.submitList(currentList)
            }
        }
        //scrollToPosition(0)
    }

    /**
     * This in function handle onScroll event of recyclerView
     * @return void
     */
    override fun onScrolled(dx: Int, dy: Int) {
        super.onScrolled(dx, dy)
        //Because our recyclerView is HORIZONTAL
        //So we only care to handle the @param dx

        //This is current list of Adapter
        val currentList = (this.adapter as LabelAdapter).currentList
        //When User scroll, we get the first position visible
        val firstPositionVisible = (this.layoutManager as LinearLayoutManager)
            .findFirstVisibleItemPosition()
        //dx != mean user start scroll HORIZONTAL
        if (dx != 0) {
            if (firstPositionVisible != NO_POSITION) {
                // First part of the List from first visible pos to size of list
                val firstPart = currentList.subList(firstPositionVisible, currentList.size)
                // Second part of the List from first item pos to first visible pos
                val secondPart = currentList.subList(0, firstPositionVisible)
                //Combine to part then submit the list
                val totalList = firstPart + secondPart
                (adapter as LabelAdapter).submitList(totalList)
            }
        }
    }

}