package com.example.learningforfun.common.view

import android.content.Context
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.PI
import kotlin.math.acos
import kotlin.math.sin

//This is Curve Demo, not available now (Update Later)
class MyLayoutManager (private val context : Context, private var horizontalScrollOffSet: Int = 0, private val attachToRoot: Boolean) : LinearLayoutManager(context,horizontalScrollOffSet,attachToRoot){

    override fun generateDefaultLayoutParams(): RecyclerView.LayoutParams {
         return RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun canScrollHorizontally(): Boolean {
        return true
    }

    override fun scrollHorizontallyBy(
        dx: Int,
        recycler: RecyclerView.Recycler?,
        state: RecyclerView.State?
    ): Int {
        horizontalScrollOffSet += dx
        fill(recycler, state)
        return dx
    }


    override fun onLayoutChildren(recycler: RecyclerView.Recycler?, state: RecyclerView.State?) {
        super.onLayoutChildren(recycler, state)
        fill(recycler,state)
    }

    private fun fill(recycler: RecyclerView.Recycler?, state: RecyclerView.State?) {
        detachAndScrapAttachedViews(recycler ?: return)
        for (itemIndex in 0 until itemCount) {
            val view = recycler.getViewForPosition(itemIndex)
            addView(view)

            val viewWidth = pxFromDp(90f)
            val viewHeight = pxFromDp(90f)

            val left = (itemIndex * viewWidth) - horizontalScrollOffSet
            val right = left + viewWidth
            val top = computeYComponent((left + right) / 2, viewHeight)
            val bottom = top.first + viewHeight

            val alpha = top.second
            view.rotation = (alpha * (180 / PI)).toFloat() - 90f
            measureChildWithMargins(view ?: return, viewWidth.toInt(), viewHeight.toInt())
            layoutDecoratedWithMargins(
                view,
                5,
                top.first,
                right.toInt(),
                bottom.toInt(),
            )
        }
        recycler.scrapList.toList().forEach {
            recycler.recycleView(it.itemView)
        }
    }

    private fun pxFromDp(dp : Float) : Float {
        return dp * context.resources.displayMetrics.density
    }

    private fun computeYComponent(viewCenterX : Float, h : Float) : Pair<Int, Double> {
        val screenWidth = context.resources.displayMetrics.widthPixels
        val s = screenWidth.toDouble() / 2
        val radius = (h * h + s * s) / (h /2)

        val xScreenFraction = viewCenterX.toDouble() / screenWidth.toDouble()
        val beta = acos(s / radius)

        val alpha = beta + (xScreenFraction * (Math.PI - (2 * beta)))
        val yComponent = radius - (radius * sin(alpha))

        return Pair(yComponent.toInt(), alpha)
    }

}