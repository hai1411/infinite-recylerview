package com.example.learningforfun.common.view

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.RelativeLayout
import com.example.learningforfun.model.Label

//This is layout Demo for Curve LayoutManager, not available (Update Later)
class MyCustomLayout@JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : RelativeLayout(context, attrs, defStyleAttr) {
}