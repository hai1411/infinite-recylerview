package com.example.learningforfun.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.learningforfun.R
import com.example.learningforfun.model.Label

class LabelAdapter : ListAdapter<Label, LabelAdapter.LabelItemViewHolder>(LabelDiffCallBack()) {

     inner class LabelItemViewHolder(view : View) : RecyclerView.ViewHolder(view) {
        private val txvContent : TextView = itemView.findViewById(R.id.txvContent)
        fun bind(label : Label) {
            txvContent.text = label.content
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LabelItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.label_item, parent,false)
        return LabelItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: LabelItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


}

class LabelDiffCallBack : DiffUtil.ItemCallback<Label>() {
    override fun areItemsTheSame(oldItem: Label, newItem: Label): Boolean {
        return oldItem.content == newItem.content
    }

    override fun areContentsTheSame(oldItem: Label, newItem: Label): Boolean {
        return oldItem == newItem
    }
}