package com.example.learningforfun

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.learningforfun.adapter.LabelAdapter
import com.example.learningforfun.common.view.MyRecyclerView
import com.example.learningforfun.model.Label

class MainActivity : AppCompatActivity() {

    private var rclLabel: MyRecyclerView? = null
    private var labelAdapter: LabelAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
    }

    private fun initView() {
        initLabelRecyclerView()
    }

    private fun initLabelRecyclerView() {
        val labelList = ArrayList<Label>()
        //The current maximum item is 10000
        //But there is a price :3
        for (i in 0..100000) {
            labelList.add(Label("Hello$i"))
        }
        labelAdapter = LabelAdapter()
        rclLabel = findViewById(R.id.rclLabel)
        rclLabel?.inputList(labelList)
        rclLabel?.layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.HORIZONTAL, false)
        rclLabel?.adapter = labelAdapter
    }
}

